resource "azurerm_resource_group" "iac_workflow" {
  name     = join("-", [var.prefix, var.environment])
  location = var.location
}

resource "azurerm_container_group" "aci_iac_workflow" {
  name                = join("-", [var.prefix, var.app_name])
  location            = azurerm_resource_group.iac_workflow.location
  resource_group_name = azurerm_resource_group.iac_workflow.name
  ip_address_type     = var.ip_address_type
  dns_name_label      = join("-", [var.prefix, var.app_name])
  os_type             = var.os_type

  container {
    name   = var.container_name
    image  = var.container_image
    cpu    = var.container_cpu
    memory = var.container_memory
    ports {
      port     = var.container_port
      protocol = var.container_protocol
    }
  }

  tags = {
    environment = var.environment
  }
}