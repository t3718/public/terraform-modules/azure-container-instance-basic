variable "prefix" {
  type        = string
  description = "The prefix used for all resources in this module"
  default     = "prefix"
}

variable "location" {
  type        = string
  description = "The Azure location where all resources in this module should be created"
  default     = "location"
}

variable "environment" {
  type        = string
  description = "The environment name"
  default     = "int"
}

variable "app_name" {
  type        = string
  description = "The application name"
  default     = "aci-example"
}

variable "ip_address_type" {
  type        = string
  description = "ip address type"
  default     = "public"
}

variable "os_type" {
  type        = string
  description = "OS type where to orchestrate the containers"
  default     = "linux"
}

variable "container_name" {
  type        = string
  description = "The docker container name"
  default     = "webtop"
}

variable "container_image" {
  type        = string
  description = "The docker container image"
  default     = "lscr.io/linuxserver/webtop:ubuntu-xfce"
}

variable "container_cpu" {
  type        = string
  description = "The docker container cpu"
  default     = "0.5"
}

variable "container_memory" {
  type        = string
  description = "The docker container memory (RAM)"
  default     = "1.5"
}

variable "container_port" {
  type        = string
  description = "The docker container port"
  default     = "80"
}

variable "container_protocol" {
  type        = string
  description = "The docker container protocol TCP or UDP"
  default     = "TCP"
}

