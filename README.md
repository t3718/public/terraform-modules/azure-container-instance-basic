[![pipeline status](https://gitlab.com/t3718/public/terraform-modules/azure-container-instance-basic/badges/main/pipeline.svg)](https://gitlab.com/t3718/public/terraform-modules/azure-container-instance-basic/-/commits/main) 

<!-- BEGIN_TF_DOCS -->

## Requirements

| Name | Version |
|------|---------|
| <a name="provider_azurerm"></a> [azurerm](#provider\_azurerm) | =2.46.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_azurerm"></a> [azurerm](#provider\_azurerm) | =2.46.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [azurerm_container_group.aci_iac_workflow](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/container_group) | resource |
| [azurerm_resource_group.iac_workflow](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/resource_group) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_app_name"></a> [app\_name](#input\_app\_name) | The application name | `string` | `"aci-example"` | no |
| <a name="input_container_cpu"></a> [container\_cpu](#input\_container\_cpu) | The docker container cpu | `string` | `"0.5"` | no |
| <a name="input_container_image"></a> [container\_image](#input\_container\_image) | The docker container image | `string` | `"lscr.io/linuxserver/webtop:ubuntu-xfce"` | no |
| <a name="input_container_memory"></a> [container\_memory](#input\_container\_memory) | The docker container memory (RAM) | `string` | `"1.5"` | no |
| <a name="input_container_name"></a> [container\_name](#input\_container\_name) | The docker container name | `string` | `"webtop"` | no |
| <a name="input_container_port"></a> [container\_port](#input\_container\_port) | The docker container port | `string` | `"80"` | no |
| <a name="input_container_protocol"></a> [container\_protocol](#input\_container\_protocol) | The docker container protocol TCP or UDP | `string` | `"TCP"` | no |
| <a name="input_environment"></a> [environment](#input\_environment) | The environment name | `string` | `"int"` | no |
| <a name="input_ip_address_type"></a> [ip\_address\_type](#input\_ip\_address\_type) | ip address type | `string` | `"public"` | no |
| <a name="input_location"></a> [location](#input\_location) | The Azure location where all resources in this module should be created | `string` | `"location"` | no |
| <a name="input_os_type"></a> [os\_type](#input\_os\_type) | OS type where to orchestrate the containers | `string` | `"linux"` | no |
| <a name="input_prefix"></a> [prefix](#input\_prefix) | The prefix used for all resources in this module | `string` | `"prefix"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_azurerm_resource_group_location"></a> [azurerm\_resource\_group\_location](#output\_azurerm\_resource\_group\_location) | The Azure location where all resources in this module were created |
| <a name="output_azurerm_resource_group_name"></a> [azurerm\_resource\_group\_name](#output\_azurerm\_resource\_group\_name) | The Azure resource group name where all resources in this module were created |
| <a name="output_fqdn"></a> [fqdn](#output\_fqdn) | the dns fqdn of the container group if dns\_name\_label is set |
| <a name="output_ip_address"></a> [ip\_address](#output\_ip\_address) | the ip address of the container group if dns\_name\_label is set |

<!-- END_TF_DOCS -->
