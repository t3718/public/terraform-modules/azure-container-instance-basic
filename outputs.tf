output "ip_address" {
  value       = "${azurerm_container_group.aci_iac_workflow.ip_address}:${var.container_port}"
  description = "the ip address of the container group if dns_name_label is set"
}

output "fqdn" {
  value       = "http://${azurerm_container_group.aci_iac_workflow.fqdn}:${var.container_port}"
  description = "the dns fqdn of the container group if dns_name_label is set"
}

output "azurerm_resource_group_name" {
  value       = azurerm_resource_group.iac_workflow.name
  description = "The Azure resource group name where all resources in this module were created"
}

output "azurerm_resource_group_location" {
  value       = azurerm_resource_group.iac_workflow.location
  description = "The Azure location where all resources in this module were created"
}